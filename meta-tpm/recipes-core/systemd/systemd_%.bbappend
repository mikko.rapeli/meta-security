PACKAGECONFIG:append = " ${@bb.utils.contains('DISTRO_FEATURES', 'tpm2', 'tpm2', '', d)}"

# for meta-arm ftpm:
#PACKAGECONFIG += "${@bb.utils.contains('MACHINE_FEATURES', 'optee-ftpm', 'tpm2', '', d)}"

# for encrypted rootfs
PACKAGECONFIG:append = " ${@bb.utils.contains('DISTRO_FEATURES', 'tpm2', 'repart openssl cryptsetup', '', d)}"
